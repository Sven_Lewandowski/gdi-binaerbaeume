#include <iostream>
#include "RBBinaryTree.h"
#include "BasicBinaryTree.h"
#include <chrono>
#include <random>
#include <unordered_set>

using namespace std;

unordered_set<int> generateRandom(unsigned int numValues)
{
    random_device randomizer;
    mt19937 generator(randomizer());
    uniform_int_distribution<> distribution(0, INT_MAX);

    unordered_set<int> keys;
    cout<<"Zufallszahlen werden generiert...\n";
    while(keys.size() < numValues)
    {
        keys.insert(distribution(generator));
    }
    cout<<"Fertig! Dies hat keinen Einfluss auf die Zeit genommen.\n";
    return keys;
}

//Nicht-zufälliges Einfügen von 1 bis numValues
RBBinaryTree orderedInsertion(unsigned int numValues)
{
    RBBinaryTree test(1);
    for(int i = 2; i<= numValues; i++)
    {
        test.insertNodeRB(i);
    }
    return test;
}

BasicBinaryTree orderedInsertionBasic(unsigned int numValues)
{
    BasicBinaryTree test(1);
    for(int i = 2; i<= numValues; i++)
    {
        test.insertNode(i);
    }
    return test;
}


RBBinaryTree setInsertion(unordered_set<int> keys)
{
    RBBinaryTree test(rand());
    for(int key : keys)
    {
        test.insertNodeRB(key);
    }
    return test;
}

BasicBinaryTree setInsertionBasic(unordered_set<int> keys)
{
    BasicBinaryTree test(rand());
    for(int key : keys)
    {
        test.insertNode(key);
    }
    return test;
}

RBBinaryTree userInsertion()
{
    int num;
    char yn;
    cout<<"Zahl hinzufuegen:\n";
    cin>>num;

    RBBinaryTree test(num);
    while(true)
    {
     cout<<"Zahl hinzufuegen:\n";
     cin>>num;
     test.insertNodeRB(num);
     test.printTree(test.root,0);
     num = 0;
     cin.clear();
     cin.ignore();
     cerr<<"Ungueltige Eingabe.\n";

    cout<<"Fortfahren?\n";
    cin>>yn;
    if(yn == 'n')
        break;
    }
    return test;
}

void randomSearch(unordered_set<int> randomNumbersInsert, unordered_set<int> randomNumbersAccess)
{
    chrono::steady_clock::time_point begin;
    chrono::steady_clock::time_point end;

    cout<<"Zuerst wird ein Baum generiert.\n";
    RBBinaryTree searchTree = setInsertion(randomNumbersInsert);

    cout<<"Fertig. Zeitmessung beignnt..\n";

    begin = chrono::steady_clock::now();
    int count = 0;
    for(int key : randomNumbersAccess)
    {
        if(searchTree.search(searchTree.root, key) != searchTree.nil)
        {
            //cout<<"Wert "<<key <<" ist vorhanden!\n";
            count++;
        }
        else
        {
            //cout<<"Wert "<<key <<" ist nicht vorhanden!\n";
        }
    }
    end = chrono::steady_clock::now();
    cout << "Fertig! Verbrauchte Zeit: = " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms\n";
    cout << "Es wurden " <<count <<" Werte erfolgreich gefunden.\n";
}

void randomSearchBasic(unordered_set<int> randomNumbersInsert, unordered_set<int> randomNumbersAccess)
{
    chrono::steady_clock::time_point begin;
    chrono::steady_clock::time_point end;

    cout<<"Zuerst wird ein Baum generiert.\n";
    BasicBinaryTree searchTree = setInsertionBasic(randomNumbersInsert);
    cout<<"Fertig. Zeitmessung startet.\n";

    begin = chrono::steady_clock::now();
    int count = 0;
    for(int key : randomNumbersAccess)
    {
        if(searchTree.search(searchTree.root, key) != searchTree.nil)
        {
            //cout<<"Wert "<<key <<" ist vorhanden!\n";
            count++;
        }
        else
        {
            //cout<<"Wert "<<key <<" ist nicht vorhanden!\n";
        }
    }
    end = chrono::steady_clock::now();
    cout << "Fertig! Verbrauchte Zeit: = " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms\n";
    cout << "Es wurden " <<count <<" Werte erfolgreich gefunden.\n";
}

void randomDelete(unordered_set<int> randomNumbersInsert, unordered_set<int> randomNumbersAccess)
{
    chrono::steady_clock::time_point begin;
    chrono::steady_clock::time_point end;
    unordered_set<int> randomNumbers;

    cout<<"Zuerst wird ein Baum generiert.\n";
    RBBinaryTree searchTree = setInsertion(randomNumbersInsert);
    cout<<"Fertig. Zeitmessung startet jetzt.\n";
    begin = chrono::steady_clock::now();

    int count = 0;

    for(int key : randomNumbersAccess)
    {
        if(searchTree.deleteNodeRB(searchTree.search(searchTree.root, key)))
        {
            count++;
        }
        else
        {
            //cout<<"Wert "<<key <<" konnte nicht entfernt werden!\n";
        }
    }
    end = chrono::steady_clock::now();
    cout << "Fertig! Verbrauchte Zeit: = " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms\n\n\n";
    cout << "Es wurde/n " <<count <<" Knoten entfernt.\n";

}

void randomDeleteBasic(unordered_set<int> randomNumbersInsert, unordered_set<int> randomNumbersAccess)
{
    chrono::steady_clock::time_point begin;
    chrono::steady_clock::time_point end;

    cout<<"Zuerst wird ein Baum generiert.\n";
    BasicBinaryTree searchTree = setInsertionBasic(randomNumbersInsert);
    cout<<"Fertig! Zeitmessung beginnt jetzt.\n";
    begin = chrono::steady_clock::now();

    int count = 0;

    for(int key : randomNumbersAccess)
    {
        if(searchTree.deleteNode(searchTree.search(searchTree.root, key)))
        {
            count++;
        }
        else
        {
            //cout<<"Wert "<<key <<" konnte nicht entfernt werden!\n";
        }
    }
    end = chrono::steady_clock::now();
    cout << "Fertig! Verbrauchte Zeit: = " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms\n\n\n";
    cout << "Es wurde/n " <<count <<" Knoten entfernt.\n";

}

void userDelete()
{
    int toDelete = 0;
    RBBinaryTree searchTree = orderedInsertion(15);
    while(toDelete != -9999)
    {
        searchTree.printTree(searchTree.root, 0);
        cout<<"Welcher Wert soll entfernt werden? -9999 ist zum Verlassen vorbestimmt.";
        cin>>toDelete;
        if(toDelete != -9999)
            searchTree.deleteNodeRB(searchTree.search(searchTree.root, toDelete));
    }
}



int main()
{
    chrono::steady_clock::time_point begin;
    chrono::steady_clock::time_point end;
    unordered_set<int> randomNumbersInsert;
    unordered_set<int> randomNumbersAccess;
    const int numValues = 3E7;

    int choice;
    while(true)
    {
        cout<<"Welche Version soll getestet werden?\n";
        cout<<"Optionen: \n1: Zeitgemessener Einfügetest mit aufsteigenden Zahlen\n";
        cout<<"2: Zeitgemessener Einfügetest mit Zufallszahlen\n";
        cout<<"3: Einfügetest mit selbstgewählten Zahlen (Zur Überprüfung der Korrektheit etc.)\n";
        cout<<"4: Suche nach zufälligen Einträgen\n";
        cout<<"5: Löschen von zufälligen Einträgen\n";
        cout<<"6: Löschen von beliebigen Einträgen\n";
        cout<<"9: Programm verlassen\n";
        cin>>choice;

        switch(choice)
        {
            case 1:
                begin = chrono::steady_clock::now();
                orderedInsertion(numValues);
                end = chrono::steady_clock::now();
                cout << "Fertig! Verbrauchte Zeit: = " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms" << endl;

                begin = chrono::steady_clock::now();
                orderedInsertionBasic(numValues);
                end = chrono::steady_clock::now();
                cout << "Fertig! Verbrauchte Zeit: = " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms" << endl;
                break;
            case 2:
                for(int i = 0; i<5; i++)
                {
                randomNumbersInsert = generateRandom(numValues);
                begin = chrono::steady_clock::now();
                setInsertion(randomNumbersInsert); //Zahlen werden separat generiert, damit Funktion für andere Sets verwendet werden kann
                end = chrono::steady_clock::now();
                cout << "Fertig! Verbrauchte Zeit: = " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms" << endl;

                begin = chrono::steady_clock::now();
                setInsertionBasic(randomNumbersInsert);
                end = chrono::steady_clock::now();
                cout << "Fertig! Verbrauchte Zeit: = " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << " ms" << endl;
                }
                break;
            case 3:
                userInsertion();
                break;
            case 4:
                for(int i = 0; i<5; i++) {
                    randomNumbersInsert = generateRandom(numValues);
                    randomNumbersAccess = generateRandom(numValues);

                    randomSearch(randomNumbersInsert, randomNumbersAccess);
                    randomSearchBasic(randomNumbersInsert, randomNumbersAccess);
                }
                break;
            case 5:
                for(int i = 0; i<5; i++) {
                    randomNumbersInsert = generateRandom(numValues);
                    randomNumbersAccess = generateRandom(numValues);

                    randomDelete(randomNumbersInsert, randomNumbersAccess);
                    randomDeleteBasic(randomNumbersInsert, randomNumbersAccess);
                }
                break;
            case 6:
                userDelete();
                break;
            case 9:
                return 0;
            default:
                cin.clear();
                cin.ignore();
                cerr<<"Ungueltige Eingabe.\n";
                continue;
        }
    }
}

