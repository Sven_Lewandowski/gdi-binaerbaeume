//
// Created by lewan on 17.01.2023.
//

#ifndef REDBLACKTREE_BINARYTREE_H
#define REDBLACKTREE_BINARYTREE_H

#include "BinaryTreeNode.h"

class BinaryTree {
    //color: 0 = black, 1 = red
public:
    BinaryTreeNode *nil;
    BinaryTreeNode *root;

    explicit BinaryTree(int key);
    BinaryTreeNode *newNode(int key);
    BinaryTreeNode *insertNode(int key, BinaryTreeNode *root);
    BinaryTreeNode *insertNodeRB(int key);
    void insertNodeRBFixup(BinaryTreeNode *node);
    BinaryTreeNode *search(BinaryTreeNode *node, int key);
    BinaryTreeNode *minimum(BinaryTreeNode *root);
    BinaryTreeNode *maximum(BinaryTreeNode *root);
    BinaryTreeNode *predecessor(BinaryTreeNode *node);
    BinaryTreeNode *successor(BinaryTreeNode *node);
    void transplant(BinaryTreeNode *root, BinaryTreeNode *oldParent, BinaryTreeNode *newParent);
    void transplantRB(BinaryTreeNode *oldParent, BinaryTreeNode *newParent);
    void left_rotate(BinaryTreeNode *node);
    void right_rotate(BinaryTreeNode *node);
    void deleteNode(BinaryTreeNode *root, BinaryTreeNode *toDelete);
    void deleteNodeRB(BinaryTreeNode *toDelete);
    void deleteNodeRBFixup(BinaryTreeNode *child);
    void printTree(BinaryTreeNode *root, int depth);
};

#endif //REDBLACKTREE_BINARYTREE_H
