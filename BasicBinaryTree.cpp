
#include <cstdlib>
#include <iostream>
#include "BasicBinaryTree.h"


BasicBinaryTree::BasicBinaryTree(int key)
{
    this->nil = new BinaryTreeNode();
    this->nil->key = -9999;

    BinaryTreeNode *node = (class BinaryTreeNode*)malloc(sizeof(class BinaryTreeNode));
    this->root = node;

    node->key = key;
    node->parent = node->left = node->right = this->nil;
}

BinaryTreeNode *BasicBinaryTree::insertNode(int key)
{
    BinaryTreeNode *node = new BinaryTreeNode();
    node->key = key;
    node->left = node->right = this->nil;

    BinaryTreeNode *iterator = this->root;

    if(search(this->root, key) != this->nil)
    {
        std::cerr<< "Wert "<<key << " ist bereits vorhanden.\n";
        return this->nil;
    }

    while(true)
    {
        if(iterator->key > node->key)
        {
            if(iterator->left != this->nil)
            {
                iterator = iterator->left;
            }
            else
            {
                iterator->left = node;
                node->parent = iterator;
                break;
            }
        }
        else
        {
            if(iterator->right != this->nil)
            {
                iterator = iterator->right;
            }
            else
            {
                iterator->right = node;
                node->parent = iterator;
                break;
            }
        }
    }
    return node;
}

void BasicBinaryTree::printTree(BinaryTreeNode *root, int depth = 0)
{
    if(root->right != this->nil)
    {
        printTree(root->right, depth+1);
    }
    std::printf("%*d%c\n", depth*4, root->key);
    if(root->left != this->nil)
    {
        printTree(root->left, depth+1);
    }
}


BinaryTreeNode *BasicBinaryTree::search(BinaryTreeNode *node, int key) {
    while(node != this->nil && node->key != key)
    {
        if(key < node->key)
            node = node->left;
        else
            node = node->right;
    }
    return node;
}

BinaryTreeNode *BasicBinaryTree::minimum(BinaryTreeNode *root)
{
    while (root->left != this->nil)
    {
        root = root->left;
    }
    return root;
}

BinaryTreeNode *BasicBinaryTree::maximum(BinaryTreeNode *root)
{
    while (root->right != this->nil)
    {
        root = root->right;
    }
    return root;
}

BinaryTreeNode *BasicBinaryTree::successor(BinaryTreeNode *node)
{
    if(node->right != this->nil)
    {
        return BasicBinaryTree::minimum(node->right);
    }
    BinaryTreeNode *parent = node->parent;
    while (parent != this->nil && node == parent->right)
    {
        node = parent;
        parent = parent->parent;
    }
    return parent;
}

BinaryTreeNode *BasicBinaryTree::predecessor(BinaryTreeNode *node)
{
    if(node->left != this->nil)
    {
        return maximum(node->left);
    }
    BinaryTreeNode *parent = node->parent;
    while (parent != this->nil && node == parent->left)
    {
        node = parent;
        parent = parent->parent;
    }
    return parent;
}

void BasicBinaryTree::transplant(BinaryTreeNode *root, BinaryTreeNode *oldParent, BinaryTreeNode *newParent)
{
    if(oldParent->parent == this->nil)
    {
        root = newParent;
    }
    else if(oldParent == oldParent->parent->left)
    {
        oldParent->parent->left = newParent;
    }
    else
    {
        oldParent->parent->right = newParent;
    }
    if(newParent != this->nil)
    {
        newParent->parent = oldParent->parent;
    }
}


bool BasicBinaryTree::deleteNode(BinaryTreeNode *toDelete)
{
    if(toDelete == this->nil)
    {
        return false;
    }
    else
    {
        if(toDelete->left == this->nil)
        {
            transplant(this->root, toDelete, toDelete->right);
        }
        else if (toDelete->right == this->nil)
        {
            transplant(this->root, toDelete, toDelete->left);
        }
        else
        {
            BinaryTreeNode *node = minimum(toDelete->right);
            if(node->parent != toDelete)
            {
                transplant(root, node, node->right);
                node->right = toDelete->right;
                node->right->parent = node;
            }
            transplant(this->root, toDelete, node);
            node->left = toDelete->left;
            node->left->parent = node;
        }
        return true;
    }
}