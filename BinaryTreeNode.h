#ifndef BINARYTREENODE_H
#define BINARYTREENODE_H


class BinaryTreeNode {
    //color: 0 = black, 1 = red
public: int key;
    char color;
    BinaryTreeNode *parent, *left, *right;
};


#endif //BINARYTREENODE_H