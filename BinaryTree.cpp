
#include <cstdlib>
#include <iostream>
#include "BinaryTree.h"

BinaryTree::BinaryTree(int key)
{
    this->nil->key = -9999;
    this->nil->color = 'B';

    BinaryTreeNode *node = (class BinaryTreeNode*)malloc(sizeof(class BinaryTreeNode));
    this->root = node;

    node->key = key;
    node->color = 'B';
    node->parent = node->left = node->right = this->nil;
}

BinaryTreeNode *BinaryTree::insertNode(int key, BinaryTreeNode *root)
{
    BinaryTreeNode *node = (class BinaryTreeNode*)malloc(sizeof(class BinaryTreeNode));
    node->key = key;
    node->color = 'R';
    node->left = node->right = nullptr;

    if(search(root, key) != nullptr)
    {
        std::cerr<< "Wert "<<key << " ist bereits vorhanden.\n";
        return nullptr;
    }

    while(true)
    {
        if(root->key > node->key)
        {
            if(root->left != nullptr)
            {
                root = root->left;
            }
            else
            {
                root->left = node;
                node->parent = root;
                break;
            }
        }
        else
        {
            if(root->right != nullptr)
            {
                root = root->right;
            }
            else
            {
                root->right = node;
                node->parent = root;
                break;
            }
        }
    }
    return node;
}

BinaryTreeNode *BinaryTree::insertNodeRB(int key)
{
    BinaryTreeNode *node = (class BinaryTreeNode*)malloc(sizeof(class BinaryTreeNode));
    node->key = key;
    node->color = 'R';
    node->left = node->right = this->nil;

    BinaryTreeNode *iterator = this->root;

    if(search(iterator, key) != this->nil)
    {
        std::cerr<< "Wert "<<key << " ist bereits vorhanden.\n";
        return nullptr;
    }

    while(iterator != this->nil)
    {
        if(iterator->key > node->key)
        {
            if(iterator->left != this->nil)
            {
                iterator = iterator->left;
            }
            else
            {
                iterator->left = node;
                node->parent = iterator;
                break;
            }
        }
        else
        {
            if(iterator->right != this->nil)
            {
                iterator = iterator->right;
            }
            else
            {
                iterator->right = node;
                node->parent = iterator;
                break;
            }
        }
    }
    insertNodeRBFixup(node);
    return node;
}

void BinaryTree::insertNodeRBFixup(BinaryTreeNode *node)
{
    while(node->parent->color == 'R')
    {
        if(node->parent == node->parent->parent->left)
        {
            BinaryTreeNode *uncle = node->parent->parent->right;
            if(uncle->color == 'R')
            {
                node->parent->color = 'B';
                uncle->color = 'B';
                node->parent->parent->color = 'R';
                node = node->parent->parent;
            }
            else
            {
                if(node == node->parent->right)
                {
                    node = node->parent;
                    left_rotate(node);
                }
            node->parent->color = 'B';
            node->parent->parent->color = 'R';
            right_rotate(node->parent->parent);
            }
        }
        else
        {
            BinaryTreeNode *uncle = node->parent->parent->left;
            if(uncle->color == 'R')
            {
                node->parent->color = 'B';
                uncle->color = 'B';
                node->parent->parent->color = 'R';
                node = node->parent->parent;
            }
            else
            {
                if(node == node->parent->left)
                {
                    node = node->parent;
                    right_rotate(node);
                }
            node->parent->color = 'B';
            node->parent->parent->color = 'R';
            left_rotate(node->parent->parent);
            }
        }

    }
    this->root->color = 'B';
}



/*
BinaryTreeNode* BinaryTree::newNode(int key)
{
    BinaryTreeNode *node = (class BinaryTreeNode*)malloc(sizeof(class BinaryTreeNode));
    this->root = node;
    this->nil->key = -99;
    this->nil->color = 'B';
    node->key = key;
    node->color = 'B';
    node->parent = node->left = node->right = this->nil;
    return node;
}
*/

void BinaryTree::printTree(BinaryTreeNode *root, int depth = 0)
{
    if(root->right != this->nil)
    {
        printTree(root->right, depth+1);
    }
    std::printf("%*d%c\n", depth*4, root->key,root->color);
    if(root->left != nil)
    {
        printTree(root->left, depth+1);
    }
}

BinaryTreeNode *BinaryTree::search(BinaryTreeNode *node, int key) {
    while(node != this->nil && node->key != key)
    {
        if(key < node->key)
            node = node->left;
        else
            node = node->right;
    }
    return node;
}

BinaryTreeNode *BinaryTree::minimum(BinaryTreeNode *root)
{
    while (root->left != this->nil)
    {
        root = root->left;
    }
    return root;
}

BinaryTreeNode *BinaryTree::maximum(BinaryTreeNode *root)
{
    while (root->right != this->nil)
    {
        root = root->right;
    }
    return root;
}

BinaryTreeNode *BinaryTree::successor(BinaryTreeNode *node)
{
    if(node->right != this->nil)
    {
        return BinaryTree::minimum(node->right);
    }
    BinaryTreeNode *parent = node->parent;
    while (parent != this->nil && node == parent->right)
    {
        node = parent;
        parent = parent->parent;
    }
    return parent;
}

BinaryTreeNode *BinaryTree::predecessor(BinaryTreeNode *node)
{
    if(node->left != this->nil)
    {
        return maximum(node->left);
    }
    BinaryTreeNode *parent = node->parent;
    while (parent != this->nil && node == parent->left)
    {
        node = parent;
        parent = parent->parent;
    }
    return parent;
}

void BinaryTree::transplant(BinaryTreeNode *root, BinaryTreeNode *oldParent, BinaryTreeNode *newParent)
{
    if(oldParent->parent == this->nil)
    {
        *root = *newParent;
    }
    else if(oldParent == oldParent->parent->left)
    {
        oldParent->parent->left = newParent;
    }
    else
    {
        oldParent->parent->right = newParent;
    }
    if(newParent != this->nil)
    {
        newParent->parent = oldParent->parent;
    }
}

void BinaryTree::transplantRB(BinaryTreeNode *oldParent, BinaryTreeNode *newParent)
{
    if(oldParent->parent == this->nil)
    {
        this->root = newParent;
    }
    else if(oldParent == oldParent->parent->left)
    {
        oldParent->parent->left = newParent;
    }
    else
    {
        oldParent->parent->right = newParent;
    }
    newParent->parent = oldParent->parent;
}


void BinaryTree::left_rotate(BinaryTreeNode *node)
{
    BinaryTreeNode *child = node->right;
    node->right = child->left;
    if(child->left != this->nil ) //eig root.nil
        child->left->parent = node;
    child->parent = node->parent;
    if(node->parent == this->nil)
        this->root = child;
    else if(node == node->parent->left)
        node->parent->left = child;
    else
        node->parent->right = child;
    child->left = node;
    node->parent = child;
}

void BinaryTree::right_rotate(BinaryTreeNode *node)
{
    BinaryTreeNode *child = node->left;
    node->left = child->right;
    if(child->right != this->nil ) //eig root.nil
        child->right->parent = node;
    child->parent = node->parent;
    if(node->parent == this->nil)
        this->root = child;
    else if(node == node->parent->right)
        node->parent->right = child;
    else
        node->parent->left= child;
    child->right= node;
    node->parent = child;
}


void BinaryTree::deleteNode(BinaryTreeNode *root, BinaryTreeNode *toDelete)
{
    if(toDelete->left == this->nil)
    {
        transplant(root, toDelete, toDelete->right);
    }
    else if (toDelete->right == this->nil)
    {
        transplant(root, toDelete, toDelete->left);
    }
    else
    {
        BinaryTreeNode *node = minimum(toDelete->right);
        if(node->parent != toDelete)
        {
            transplant(root, node, node->right);
            *node->right = *toDelete->right;
            *node->right->parent = *node;
        }
        transplant(root, toDelete, node);
        node->left = toDelete->left;
        node->left->parent = node;
    }
}

void BinaryTree::deleteNodeRB(BinaryTreeNode *toDelete)
{
    BinaryTreeNode *replacer = toDelete;
    BinaryTreeNode *child;
    char oldColor = replacer->color;

    if(toDelete->left == this->nil)
    {
        child = toDelete->right;
        transplantRB(toDelete, toDelete->right);
    }
    else if(toDelete->right == this->nil)
    {
        child = toDelete->left;
        transplantRB(toDelete, toDelete->left);
    }
    else
    {
        replacer = minimum(toDelete->right);
        oldColor = replacer->color;
        child = replacer->right;
        if(replacer->parent == toDelete)
        {
            child->parent = replacer;
        }
        else
        {
            transplantRB(replacer, replacer->right);
            replacer->right = toDelete->right;
            replacer->right->parent = replacer;
        }
        transplantRB(toDelete, replacer);
        replacer->left = toDelete->left;
        replacer->left->parent = replacer;
        replacer->color = toDelete->color;
    }

    if(oldColor == 'B')
    {
        deleteNodeRBFixup(child);
    }
}

void BinaryTree::deleteNodeRBFixup(BinaryTreeNode *child)
{
    while(child != this->root && child->color == 'B')
    {
        if(child == child->parent->left)
        {
            BinaryTreeNode *sibling = child->parent->right;
            if(sibling->color == 'R')
            {
                sibling->color = 'B';
                child->parent->color = 'R';
                left_rotate(child->parent);
                sibling = child->parent->right;
            }
            if(sibling->left->color == 'B' && sibling->right->color == 'B')
            {
                sibling->color = 'R';
                child = child->parent;
            }
            else
            {
                if (sibling->right->color == 'B')
                {
                    sibling->left->color = 'B';
                    sibling->color = 'R';
                    right_rotate(sibling);
                    sibling = child->parent->right;
                }
                sibling->color = child->parent->color;
                child->parent->color = 'B';
                sibling->right->color = 'B';
                left_rotate(child->parent);
                child = this->root;
            }
        }
        else
        {
            BinaryTreeNode *sibling = child->parent->left;
            if(sibling->color == 'R')
            {
                sibling->color = 'B';
                child->parent->color = 'R';
                right_rotate(child->parent);
                sibling = child->parent->left;
            }
            if(sibling->right->color == 'B' && sibling->left->color == 'B')
            {
                sibling->color = 'R';
                child = child->parent;
            }
            else
            {
                if (sibling->left->color == 'B')
                {
                    sibling->right->color = 'B';
                    sibling->color = 'R';
                    left_rotate(sibling);
                    sibling = child->parent->left;
                }
                sibling->color = child->parent->color;
                child->parent->color = 'B';
                sibling->left->color = 'B';
                right_rotate(child->parent);
                child = this->root;
            }
        }
    }
    child->color = 'B';
}