//
// Created by lewan on 17.01.2023.
//

#ifndef REDBLACKTREE_BASICBINARYTREE_H
#define REDBLACKTREE_BASICBINARYTREE_H

#include "BinaryTreeNode.h"

class BasicBinaryTree {
public:
    BinaryTreeNode *nil;
    BinaryTreeNode *root;

    explicit BasicBinaryTree(int key);
    BinaryTreeNode *insertNode(int key);
    BinaryTreeNode *search(BinaryTreeNode *node, int key);
    BinaryTreeNode *minimum(BinaryTreeNode *root);
    BinaryTreeNode *maximum(BinaryTreeNode *root);
    BinaryTreeNode *predecessor(BinaryTreeNode *node);
    BinaryTreeNode *successor(BinaryTreeNode *node);
    void transplant(BinaryTreeNode *root, BinaryTreeNode *oldParent, BinaryTreeNode *newParent);
    bool deleteNode(BinaryTreeNode *toDelete);
    void printTree(BinaryTreeNode *root, int depth);
};

#endif //REDBLACKTREE_BASICBINARYTREE_H
